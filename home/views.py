from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.urls import reverse_lazy

from core.models import GrantGoal
from .models import Profile
from .forms import LoginForm, SignupForm

# Create your views here.

class Index(generic.View):
    template_name = "home/index.html"
    context = {}
    form_class = LoginForm

    def get(self, request):
        self.context = {
            "grantgoals": GrantGoal.objects.filter(status=True),
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("home:index")
        return redirect("home:index")



class Logout(generic.View):
    def get(self, request):
        logout(request)
        return redirect("home:index")
    


class Signup(generic.CreateView):
    template_name = "home/signup.html"
    model = User
    form_class = SignupForm
    success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password)
        if user is not None:
            login(self.request, user)
            return redirect("home:index")
        return redirect("home:index")
    


class ListProfile(generic.View):
    template_name = "home/list_profile.html"
    context = {}

    def get(self, request):
        self.context = {
            "profiles": Profile.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)
    


class DetailProfile(generic.View):
    template_name = "home/detail_profile.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "profile": Profile.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)
