from django import forms

from .models import GrantGoal, SubGrantGoal


class NewGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "ggname",
            "description",
            "user",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }



class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "ggname",
            "description",
            "user",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }

    

class NewSubGrantGoalForm(forms.ModelForm):
    class Meta:
        model = SubGrantGoal
        fields = [
            "sggname",
            "description",
            "user",
            "grantgoal",
            "area",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "sggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "grantgoal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }



class UpdateSubGrantGoalForm(forms.ModelForm):
    class Meta:
        model = SubGrantGoal
        fields = [
            "sggname",
            "description",
            "user",
            "grantgoal",
            "area",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "sggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "grantgoal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }